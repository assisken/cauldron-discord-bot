package main

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"math/rand"
)

func handleCathoddMessages(s *discordgo.Session, m *discordgo.MessageCreate) {
	const cathoddId = "137946707555123200"
	const turikEmoji = ":turik:801844597303017503"
	const emojiChance = 10

	if m.Author.ID != cathoddId {
		return
	}
	if rand.Intn(emojiChance) != 0 {
		return
	}

	err := s.MessageReactionAdd(m.ChannelID, m.ID, turikEmoji)
	if err != nil {
		fmt.Println(err)
		return
	}
}
